//  Q.3 Find all movies of the actor "Leonardo Dicaprio".

const favouritesMovies = require("./2-movies.cjs");
console.log(favouritesMovies);

const ans = Object.entries(favouritesMovies).filter((item) => {
    return item[1].actors.includes("Leonardo Dicaprio");
});
console.log(ans, "printing ans");
