// Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
const favouritesMovies = require("./2-movies.cjs");

const ans = Object.entries(favouritesMovies).filter((item) => {
    let totalEarning = item[1].totalEarnings.replaceAll("$", " ");
    let newTotalEarning = totalEarning.replaceAll("M", " ");
    let totalOscarNomination = item[1].oscarNominations;
    if(newTotalEarning > 500 && totalOscarNomination > 3) {
        return item;
    }
});
console.log(ans, "printing ans");
