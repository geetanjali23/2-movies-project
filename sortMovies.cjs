// Q.4 Sort movies (based on IMDB rating)
// if IMDB ratings are same, compare totalEarning as the secondary metric.

const favouritesMovies = require("./2-movies.cjs");
const ans = Object.entries(favouritesMovies).sort((item1, item2) => {
    if (item1[1].imdbRating > item2[1].imdbRating) {
        return -1;
    }
    else if (item1[1].imdbRating === item2[1].imdbRating) {
        if (item1[1].totalEarnings > item2[1].totalEarnings) {
            return -1;
        }
    }
});
console.log(ans, "Printing result ");