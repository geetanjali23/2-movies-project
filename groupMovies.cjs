// Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
// drama > sci-fi > adventure > thriller > crime
const favouritesMovies = require("./2-movies.cjs");
let genresOfFilms = Object.keys(favouritesMovies).reduce((accumulator, movieName) => {
    if (favouritesMovies[movieName].genre.includes("drama")) {
        accumulator.drama.push({ [movieName]: favouritesMovies[movieName] });
    }
    if (favouritesMovies[movieName].genre.includes("sci-fi")) {
        accumulator['sci-fi'].push({ [movieName]: favouritesMovies[movieName] });
    }
    if (favouritesMovies[movieName].genre.includes("adventure")) {
        accumulator.adventure.push({ [movieName]: favouritesMovies[movieName] });
    }
    if (favouritesMovies[movieName].genre.includes("thriller")) {
        accumulator.thriller.push({ [movieName]: favouritesMovies[movieName] });
    }
    if (favouritesMovies[movieName].genre.includes("crime")) {
        accumulator.crime.push({ [movieName]: favouritesMovies[movieName] });
    }
    return accumulator;
}, { "drama": [], 'sci-fi': [], "adventure": [], "thriller": [], "crime": [] });

console.log(genresOfFilms, "Printing result ...");

